#!/usr/bin/env bash

set -Eeu


print-usage() {
    echo "usage: $0 [-h] (pflow|trackjets) inputfile.h5" 1>&2
}

usage() {
    print-usage
    exit 1;
}

help() {
    print-usage
    cat <<EOF

The ${0##*/} utility will create documentation for the variables
contained within an h5 file as markdown files.

Options:
 -h: print help

EOF
    exit 1
}

while getopts ":h" o; do
    case "${o}" in
        h) help ;;
        *) usage ;;
    esac
done
shift $((OPTIND-1))

if (( $# != 2 )) ; then
    usage
fi

MODE=$1
INPUT_FILE=$2

# fixed variables
DOCS_DIR=$(dirname $(readlink -e ${BASH_SOURCE[0]}))

DESCRIPTIONS=${DOCS_DIR}/../data/field-descriptions.yaml

# config switch based on mode
declare -A OUTPUT_DIR=(
    [pflow]=ci-docs-pflow
    [open]=ci-docs-open
)
declare -A OUTPUT_NAME=(
    [pflow]=vars_pflow.md
    [open]=vars_open.md
)
declare -A PREAMBLE=(
    [pflow]=${DOCS_DIR}/../data/pflow_preamble.md
    [open]=${DOCS_DIR}/../data/open_preamble.md
)
declare -A EPILOGUE=(
    [pflow]=${DOCS_DIR}/../data/pflow_epilogue.md
    [open]=${DOCS_DIR}/../data/pflow_epilogue.md
)

if [ -z ${GITLAB_CI+x} ] ; then
    echo "ERROR: This script should be run inside of the gitlab CI."
else
    # bail out if running for a configuration not defined
    if [[ ! ${OUTPUT_DIR[$MODE]+x} ]]; then usage; fi
    if [[ ! ${OUTPUT_NAME[$MODE]+x} ]]; then usage; fi
    if [[ ! ${PREAMBLE[$MODE]+x} ]]; then usage; fi
    if [[ ! ${EPILOGUE[$MODE]+x} ]]; then usage; fi

    # install missing python packages
    pip install --upgrade pyyaml h5py

    # dump variables in markdown file
    mkdir -p ${OUTPUT_DIR[$MODE]}
    python docs/scripts/make_variable_docs.py \
        ${INPUT_FILE} \
        -s ${DESCRIPTIONS} \
        -p ${PREAMBLE[$MODE]} \
        -e ${EPILOGUE[$MODE]} \
        -o ${OUTPUT_DIR[$MODE]}/${OUTPUT_NAME[$MODE]}
fi
