This page contains a non exhaustive list of features of this package, and explains for which features you need set up with `AthAnalysis` versus `Athena`.

## Choosing between `AthAnalysis` and `Athena`

The standard configuration files, described in the [previous section](configuration.md), are capable of calculating and dumping simple variables from jets, b-tagging and tracks, and of running the neural network based flavor taggers. 
If you are starting from a DAOD and don't know otherwise, that is likely all the functionality you need, so you can stop reading here. 
If you want to do something more complicated, read on.

Some example of advanced features that you can perform with either `AthAnalysis` or `Athena` are:

- Building secondary vertices (i.e. "retagging") and running the full
  flavor tagging chain from untagged jets.
- Re-running track to jet association.

Some things that `Athena` supports that `AthAnalysis` does not are:

- Using the ATLAS geometry service or the ATLAS magnetic field (i.e. track extrapolation, running full b-tagging, or jet reconstruction).
- Extrapolating tracks to primary vertex to (re)calculate impact parameters
- Creating transient jet collections from trigger decisions, for trigger studies


## `AthAnalysis` Features

### Running Decoration and Augmentation Algorithms

As discussed in the [contributing guidelines](contributing.md#adding-more-outputs),
it is preferred to add decoration algorithms directly to Athena, rather than in this package.
The Athena algorithms can then be scheduled to run before a dumping job, which is useful when the 
algorithm has not already been run in the derivations.

Scheduling an Athena algorithm can be done by creating a new CA block.
This approach allows you to configure the CA blocks via the JSON config file,
rather than using the python script to effectively store the configuration in the code.

More information about creating blocks can be found [here](ca_blocks.md).


### Run on heavy ion xAODs

You need to run under athanalysis (use `setup/athanalysis.sh`).

Since HI xAOD are still produced under R21 we can dump using:

```bash
dump-upgrade-HI -c <path to configuration file> <paths to xAOD(s)>
```

The configuration file should be `upgrade-HI.json`, which will use `pflow-variables.json`. Flag `do_heavions` must be set to true in `upgrade-HI.json`.

To run on the grid use:
```bash
grid-submit upgrade-HI
```

### Run on Upgrade samples

For Upgrade DAODs produced in release 24, the default [`dump-single-btag`]({{repo_url}}-/blob/main/FTagDumper/bin/dump-single-btag) steering should be used.


## `Athena` Features

### Retagging with locally modified Athena packages

In some cases, you may need to modify a part of the flavour-tagging full chain. For instance, you may want to tune one of the secondary vertexing algorithms to identify its best working point after some change occurred in Athena. To do that, you need a tool that allows you to re-run flavour tagging on your sample [importing the changes you made on Athena packages](contributing.md#editing-athena-packages).

[`dump-retag`]({{repo_url}}-/blob/main/FTagDumper/bin/dump-retag), the script that implements retagging, is such tool.

To use it, you need to setup your environment with `setup/athena.sh`.

The code runs as follows:
```bash
dump-retag -c <path to configuration file> <paths to xAOD(s)>
```

To run on the grid, use:
```bash
grid-submit retag
```

### Run on trigger samples

You can make trigger training datasets in Athena with one of these commands

```bash
dump-trigger-pflow -c <dumper>/configs/trigger_pflow.json xAOD.root
dump-trigger-emtopo -c <dumper>/configs/trigger_emtopo.json xAOD.root
```

This is a _much_ heavier job than the standard ones, because it has to
load in a number of dependencies for track extrapolation. Depending on which config files you are using there are several other scripts that can be executed, see the `-h` for more options.

You can use `grid-submit trigger` to submit these jobs to the grid, there are also individual options, see `grid submit -h`.
