from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock

@dataclass
class PoorMansIpAugmenter(BaseBlock):
    name: str = "PoorMansIpAugmentAlg"
    prefix: str = "btagIp_"
    # Use empty to use beamspot
    primary_vertex_container : str = ""
    track_container : str = "InDetTrackParticles"

    def to_ca(self):
        ca = ComponentAccumulator()
        ca.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
                self.name,
                prefix=self.prefix,
                primaryVertexContainer=self.primary_vertex_container,
                trackContainer=self.track_container,
            )
        )
        return ca