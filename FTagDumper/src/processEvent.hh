#ifndef PROCESS_SINGLE_BTAG_EVENT_HH
#define PROCESS_SINGLE_BTAG_EVENT_HH

class JetDumperConfig;
class JetDumperTools;
class JetDumperOutputs;
namespace xAOD {
  class TEvent;
}

// the joy of "duel use": there doesn't appear to be a common
// baseclass that implements the one method we need in TEvent and
// StoreGate (i.e. `retrieve`). So we use overloads here, templates
// are defined in processEvent.tcc

void processEvent(xAOD::TEvent&,
                            const JetDumperConfig&,
                            const JetDumperTools&,
                            JetDumperOutputs&);

class StoreGateSvc;
void processEvent(StoreGateSvc&,
                  const JetDumperConfig&,
                  const JetDumperTools&,
                  JetDumperOutputs&);

#endif  // PROCESS_SINGLE_BTAG_EVENT_HH
