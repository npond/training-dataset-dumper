#ifndef JET_LEPTON_DECAY_LABEEL_DECORATOR_HH
#define JET_LEPTON_DECAY_LABEEL_DECORATOR_HH

#include "xAODJet/JetFwd.h"
#include "xAODBTagging/BTaggingContainerFwd.h"
#include "AthLinks/ElementLink.h"
#include "xAODTruth/TruthParticle.h"

#include <string>


struct LeptonCounter {
    int el_fromHad = 0;
    int mu_fromHad = 0;
    int tau_fromHad = 0;
    int el_fromTau = 0;
    int mu_fromTau = 0;
  };

class JetLeptonDecayLabelDecorator
{
public:
  JetLeptonDecayLabelDecorator(const std::string& prefix = "");

  // this is the function that actually does the decoration
  void decorate(const xAOD::Jet& jet) const;

private:
  SG::AuxElement::Decorator<int> m_decoDecay;
  SG::AuxElement::Decorator<int> m_decoTau;
                                             
  LeptonCounter countLeptons(const std::vector<const xAOD::TruthParticle*> &Had, const xAOD::Jet& jet) const; 
  int getDecayLabel(const std::vector <int> leps_in_decays) const;
  int getTauLabel(const std::vector <int> tauleps_in_decays, const std::vector <int> leps_in_decays) const;

};

#endif
